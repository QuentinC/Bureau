package admission;

/**
 *
 * @author Quentin
 */
public enum Venue {
    Urgences, ConsultationExterne, Hospitalisation
}
