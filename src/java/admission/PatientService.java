/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admission;

import bureau.Crayon;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author Quentin
 */
public class PatientService {
    
    EntityManagerFactory fact;
    EntityManager em;

    public PatientService(EntityManagerFactory fact) {
        this.fact = fact;
        this.em = fact.createEntityManager();
    }
    
    public void createPatient(Patient p){
        em.getTransaction( ).begin( );
        em.persist(p);
        em.getTransaction().commit();
    }
    
    public void updatePatient(Patient p){
        em.getTransaction().begin();
        em.merge(p);
        em.getTransaction().commit();
    }
    
    public List<Patient> getPatientByNameAndFirstnameAndBirthday(String name, String firstName, Date birthday){
        
        TypedQuery<Patient> query = em.createQuery("SELECT p FROM Patient p WHERE p.nomP = :name AND p.prenomP = :firstName AND p.dateNaiss = :birthday", Patient.class)
                .setParameter("name", name).setParameter("firstName", firstName).setParameter("birthday", birthday);
        List<Patient> p = query.getResultList();
        
        return p;
    }
    
    public List<Patient> getAllPatient(){
        
        TypedQuery<Patient> query = em.createQuery("SELECT p FROM Patient p", Patient.class);
        List<Patient> p = query.getResultList();
        return p;
    }
    
    public List<Patient> getPatientByNameOrFirstNameOrBirthday(String name, String firstName, Date birthday, 
            String adresse, String telephone, Long numSecu){
        
        TypedQuery<Patient> query = em.createQuery("SELECT p FROM Patient p WHERE p.nomP = coalesce(:name, p.nomP) "
                + "AND p.prenomP = coalesce(:firstName, p.prenomP) "
                + "AND coalesce(p.dateNaiss, 'erreur') = coalesce(:birthday, p.dateNaiss, 'erreur') "
                + "AND coalesce(p.adresse, 'erreur') = coalesce(:address, p.adresse, 'erreur') "
                + "AND coalesce(p.telephone, 'erreur') = coalesce(:phone, p.telephone, 'erreur') "
                + "AND coalesce(p.numSecu, 'erreur') = coalesce(:numSecu, p.numSecu, 'erreur')", Patient.class)
                .setParameter("name", name).setParameter("firstName", firstName).setParameter("birthday", birthday)
                .setParameter("adresse", adresse).setParameter("telephone", telephone).setParameter("numSecu", numSecu);
        List<Patient> p = query.getResultList();
        return p;
    }
}
