/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admission;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sushi
 */
public class AdmissionService {
    
    EntityManagerFactory fact;
    EntityManager em;
    
    public AdmissionService(EntityManagerFactory fact){
        this.fact = fact;
        this.em = fact.createEntityManager();
    }
    
    public void newAdmission(Date entree,Venue venue){
    Admission a = new Admission(entree, venue);
    em.getTransaction().begin();
    em.persist(a);
    em.getTransaction().commit();
    }

    public void updateAdmission(Date sortie, Long iep){
    Admission res = em.find( Admission.class, iep );
    res.setDateSortieA(sortie);
    }
    
    public List<Admission> fndAdmission(Long iep, Date entree,Date sortie){
    
    TypedQuery<Admission>  query = (TypedQuery<Admission>) em.createQuery("SELECT * FROM admission a WHERE iep= coalesce(:id, iep)"
            + " AND dateEntreA = coalesce(:entree, dateEntreA) "
            + " AND dateSortieA= coalesce (:sortie, 'rien')")
                .setParameter("id",iep).setParameter("entree", entree).setParameter("sortie", sortie);
    List<Admission> res = query.getResultList();
    return res;
    }
}
