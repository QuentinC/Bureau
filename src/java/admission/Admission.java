package admission;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Quentin
 */
@Entity
public class Admission implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long iEP;
    
    @Column
    private Date dateEntreA;
    
    @Column
    private Date dateSortieA;
    
    @Column
    private Venue venue;
    
    @Column
    @ManyToOne
    private Patient patient;

    public Admission(Date dateEntreA, Venue venue) {
        this.dateEntreA = dateEntreA;
        this.venue = venue;
    }

    
    
    public Long getIEP() {
        return iEP;
    }

    public void setIEP(Long iEP) {
        this.iEP = iEP;
    }

    public Date getDateEntreA() {
        return dateEntreA;
    }

    public void setDateEntreA(Date dateEntreA) {
        this.dateEntreA = dateEntreA;
    }

    public Date getDateSortieA() {
        return dateSortieA;
    }

    public void setDateSortieA(Date dateSortieA) {
        this.dateSortieA = dateSortieA;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }
    
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iEP != null ? iEP.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the IEP fields are not set
        if (!(object instanceof Admission)) {
            return false;
        }
        Admission other = (Admission) object;
        if ((this.iEP == null && other.iEP != null) || (this.iEP != null && !this.iEP.equals(other.iEP))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "admission.Admission[ iEP=" + iEP + " ]";
    }
    
}
